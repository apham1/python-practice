
year1 = input("Enter a year: ")
adjective1 = input("Enter an adjective: ")
number = input("Enter a number: ")
number_below_51 = input("Enter a number below 51: ")
number_below_196 = input("Enter a number below 196: ")
adjective2 = input("Enter an adjective: ")
historical_figure = input("Enter a historical figure: ")
year2 = input("Enter a year: ")
random_name = input("Enter a name: ")
beloved_celebrity = input("Enter a beloved celebrity: ")
verb = input("Enter a verb: ")
noun1 = input("Enter a noun: ")
adjective3 = input("Enter an adjective: ")
occupation1 = input("Enter an occupation: ")
occupation2 = input("Enter another occupation: ")
noun2 = input("Enter a noun: ")

print("We've gathered here today to celebrate the class of " + year1 + 
      ", a " + adjective1 + 
      " group of " + number + 
      "students hailing from " + number_below_51 + 
      " states and " + number_below_196 + 
      " countries to create a " + adjective2 +
      " community like nowhere else. Since " + historical_figure +
      " founded the school in " + year2 +
      ", it's produced students like " + random_name +
      " and " + beloved_celebrity +
      ", people who understand what our motto means when it tells us to \"" + verb +
      " the " + noun1 +
      " " + adjective3 +
      ".\" We know you will do the same, because even if you go on to be a " + occupation1 +
      " or " + occupation2 +
      ", you all share one thing in common: a " + noun2 + " from this fine institution.")