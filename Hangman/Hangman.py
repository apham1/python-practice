import random

easy_words = ["about", "active",
              "baby", "boat",
              "cake", "cloud",
              "dance", "dark",
              "every", "early",
              "face", "fact",
              "game", "gift",
              "hair", "hammer",
              "idea", "inside",
              "jelly", "jump",
              "keep", "knife",
              "ladder", "letter",
              "page", "plastic",
              "queen", "quick",
              "radio", "record",
              "school", "search",
              "table", "tennis",
              "ugly", "useful",
              "village", "voice",
              "wait", "water",
              "yard", "young",
              "zero", "zoo"]

hard_words = ["accommodate", "handkerchief", "indict", "cemetery", "conscience", "rhythm", "playwright", 
              "embarrass", "millennium", "pharaoh", "liaison", "convalesce", "supersede", "ecstasy", 
              "Caribbean", "harass", "maintenance", "pronunciation", "deductible", "recommend"]

very_hard_words = ["impetuous", "pabulum", "engender", "intransigent", "abjure", "aioli", "xenophilia",
                  "ensconce", "calumniate", "depredation", "pusillanimous", "liturgical", "prorogue"]

def yes_no_query():
    yes = set(['yes', 'y'])
    no = set(['no', 'n'])

    while True:
        query = input("Play again? (y/n): ")
        if query.lower() in yes:
            return True
        elif query.lower() in no:
            return False
        else:
            print("Invalid input")

def difficulty_query():
    acceptable_inputs = set (['1', '2', '3'])

    while True:
        difficulty = input("Enter a difficulty (1-3): ")
        if difficulty in acceptable_inputs:
            if difficulty == '1':
                end_of_list = len(easy_words) - 1
                return easy_words[random.randint(0, end_of_list)]
            elif difficulty == '2':
                end_of_list = len(hard_words) - 1
                return hard_words[random.randint(0, end_of_list)]
            elif difficulty == '3':
                end_of_list = len(very_hard_words) - 1
                return very_hard_words[random.randint(0, end_of_list)]

while True:
    word_to_guess = difficulty_query()
    split_word = list(word_to_guess)
    word_length = len(word_to_guess)
    guess_progress = []
    letters_guessed = 0
    lives = 7

    for x in range(0, word_length):
        guess_progress.append([0, split_word[x]])
    
    while True:
        for x in range(0, word_length):
            if guess_progress[x][0] == 0:
                print("_ ", end = "")
            elif guess_progress[x][0] == 1:
                print(guess_progress[x][1] + " ", end = "")
        print("          Lives Left: %d" % lives)

        current_guess = input("Guess a letter: ")
        correct_guess = False
        for x in range(0, word_length):
            if current_guess == guess_progress[x][1]:
                correct_guess = True
                if guess_progress[x][0] == 0:
                    guess_progress[x][0] += 1
                    letters_guessed += 1
        if correct_guess == False:
            lives -= 1

        if letters_guessed == word_length:
            print("You guessed the word! (%s)" % word_to_guess)
            break
        if lives == 0:
            print("You ran out of lives!")
            break

    if yes_no_query() == False:
        break