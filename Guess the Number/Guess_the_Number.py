import random

def yes_no_query():
    yes = set(['yes', 'y'])
    no = set(['no', 'n'])

    while True:
        query = input("Play again? (y/n): ")
        if query.lower() in yes:
            return True
        elif query.lower() in no:
            return False
        else:
            print("Invalid input")

while True:
    limit = 100
    number_to_guess = random.randint(1, limit)
    print("Guess the number between 1 and", limit)
    
    while True:
        prompt = "Enter a number (1-" + str(limit) + "): "
        guess = int(input(prompt))
        if guess < number_to_guess:
            print("Too low")
        elif guess > number_to_guess:
            print("Too high")
        elif guess == number_to_guess:
            print("You guessed the number! (", number_to_guess, ")")
            break
        else:
            print("Invalid input")

    if yes_no_query() == False:
        break