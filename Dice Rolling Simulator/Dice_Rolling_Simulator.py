import random

def yes_no_query():
    yes = set(['yes', 'y'])
    no = set(['no', 'n'])

    while True:
        query = input("Play again? (y/n): ")
        if query.lower() in yes:
            return True
        elif query.lower() in no:
            return False
        else:
            print("Invalid input")

def roll_dice():
    print(random.randint(1, 6))

while True:
    roll_dice()
    if yes_no_query() == False:
        break